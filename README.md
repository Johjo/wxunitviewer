# wxUnitViewer

## Présentation

wxUnitViewer est un outil qui permet d'afficher les résultats des tests produits par le framework de test [wxUnit](https://gitlab.com/Johjo/wxunit).

## Pré-requis

wxUnitViewer est compatible avec la dernière version de wxUnit.

## Installation

* [Télécharger l'installateur](https://gitlab.com/Johjo/wxunitviewer/-/raw/master/Install_Production/wxUnitViewer.EXE)
* Exécuter l'installeur et veiller à cocher l'option **lancer l'application** une fois celle-ci installée.
* Lorsque wxUnitViewer se lance pour la première fois, cliquer sur le bouton **Associer l'extension** dans la fenêtre principale.
